-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 23, 2013 at 12:02 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `textile`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `aid` varchar(10) NOT NULL,
  `pwd` varchar(10) NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`aid`, `pwd`) VALUES
('admin', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `cid` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `addr` varchar(225) NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cid`, `name`, `phone`, `addr`) VALUES
(1, 'phani', '9394862144', 'vjw'),
(11, 'srinu', '880575646', 'krnl'),
(15, 'nikhil', '44567890', 'us'),
(16, 'phanik', '9394862145', 'hagfajh'),
(17, 'SaiLakshmi', '9494532609', 'vjw'),
(18, 'maheshji', '8019875982', 'kalikavai'),
(19, 'Jai Kum', '7456891235', 'Ya'),
(20, 'hik', '9394862145', 'vjw'),
(21, 'hi', '8596321475', 'tn'),
(22, 'ram', '8523697410', 'mid'),
(23, 'tin', '7889654123', 'ghv'),
(29, 'Yes', '7896541230', 'Vjw\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `cid` varchar(9) NOT NULL,
  `sid` varchar(10) NOT NULL,
  `sgroup` varchar(20) NOT NULL,
  `paid` double NOT NULL,
  `due` double NOT NULL,
  `lpdate` datetime NOT NULL,
  `pdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`cid`, `sid`, `sgroup`, `paid`, `due`, `lpdate`, `pdate`) VALUES
('18', '102', 'Silk', 100, 1100, '2013-11-17 05:11:57', '2013-11-17 05:11:57'),
('18', '102', 'Silk', 100, 1000, '2013-11-17 05:11:35', '2013-11-17 05:11:57'),
('18', '102', 'Silk', 100, 900, '2013-11-17 05:11:42', '2013-11-17 05:11:57'),
('18', '102', 'Silk', 500, 400, '2013-11-18 08:11:21', '2013-11-17 05:11:57'),
('18', '102', 'Silk', 100, 300, '2013-11-18 08:11:16', '2013-11-17 05:11:57'),
('18', '1', 'Cotton', 500, 500, '2013-11-18 11:11:47', '2013-11-18 11:11:47'),
('18', '102', 'Silk', 100, 200, '2013-11-20 10:11:04', '2013-11-17 05:11:57'),
('18', '102', 'Silk', 100, 100, '2013-11-20 08:11:36', '2013-11-17 05:11:57'),
('19', '245', 'Cotton', 2000, 500, '2013-11-21 02:11:45', '2013-11-21 02:11:45'),
('22', '8', 'Others', 1500, 500, '2013-11-21 02:11:46', '2013-11-21 02:11:46'),
('19', '89', 'Cotton', 200, 2100, '2013-11-21 10:11:57', '2013-11-21 10:11:57'),
('19', '1', 'Banares', 1400, 1200, '2013-11-23 17:14:44', '2013-11-23 17:14:44');

-- --------------------------------------------------------

--
-- Table structure for table `sarees`
--

CREATE TABLE IF NOT EXISTS `sarees` (
  `sid` varchar(10) NOT NULL,
  `sprice` double NOT NULL,
  `sgroup` varchar(20) NOT NULL,
  `available` varchar(1) NOT NULL DEFAULT 'a'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sarees`
--

INSERT INTO `sarees` (`sid`, `sprice`, `sgroup`, `available`) VALUES
('1', 1000, 'Cotton', 'n'),
('102', 1200, 'Silk', 'n'),
('245', 2500, 'Cotton', 'n'),
('8', 2000, 'Others', 'n'),
('89', 2300, 'Cotton', 'n'),
('1', 2600, 'Banares', 'n'),
('85', 450, 'Others', 'a'),
('65', 852, 'Others', 'a'),
('78', 522, 'Banares', 'a');

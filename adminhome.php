<?php
session_start();
if( !$_SESSION['login']== 'true') 
 header("Location:index.php");?>
<html>

  <head> <link rel="stylesheet" href="stylehome.css" type="text/css" media="screen" />
  <script type="text/javascript" src="jquery-1.10.2.min.js"></script>
  <link rel="stylesheet" href="agile_carousel/agile_carousel.css" type='text/css'>

<script src="agile_carousel/agile_carousel.js"></script>

<script>

    $.getJSON("agile_carousel/agile_carousel_data.php", function(data) {
        $(document).ready(function(){
            $("#flavor_1").agile_carousel({
                carousel_data: data,
                carousel_outer_height: ($('body').height())*0.8,
                carousel_height:  ($('body').height())*0.8,
                slide_height:  ($('body').height())*0.8,
                carousel_outer_width: $('body').width() ,
                slide_width: $('body').width(),
                transition_time: 300,
                timer: 4000,
                continuous_scrolling: true,
                control_set_1: "numbered_buttons",
                no_control_set: "hover_previous_button,hover_next_button"
            });
        });
    });
</script>
</head>
<body>

<div id="head-bar"><?php include "menuhead.html" ; ?>
</div><br><div class="slideshow" id="flavor_1"></div>
<div id="hbanner">
<?php include "hbanner.php" ?>
</div>
</body>
</html>
